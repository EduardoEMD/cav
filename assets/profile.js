var Profile = function(){

	this.bindEvents = function(){

		$('#search_button').on('input', () => {

			var target = $('.search_input').val()
			$('.searching_div').empty()


			if(target.trim() != ""){

			var data = {

				action : "search_users",
				target :  target,
			}

			Global.request('post','server.php', data, "json")
				.then(function(r){

					r.forEach(function(item){

						var elm = $('<section></section>') 

						elm.append(

							"<p>" + item.nickname + "</p>",
							`<button  data-id="${item.id}" class="request alert alert-success">add +</button>`
						)
						$('.searching_div').append(elm)

					})


				})
			}
		})

		$(document).on('click','.request',function(){

			var id 	   = $(this).data('id')

			var data   = {

				action : 'request',
				id     : id
			}

			$(this).parent().fadeOut(300)

			Global.request('post','server.php',data)
				.then(function(r){

				})


		})

		$('#requests').click(function(){

			$('.modal-body').empty()

			var data = {

				action : 'my-requests'

			}

			Global.request('post','server.php', data, 'json')
				.then(function(r){

					console.log(r)

					r.forEach(function(item){


					var elm = $('<section></section>')

					elm.append(

						"<p>" + item.nickname + "</p>",
						`<button data-answer="reject" data-id="${item.id}" class="request-answer alert alert-danger">Decline</button>`,
						`<button data-answer="accept" data-id="${item.id}" class="request-answer alert alert-success">Accept</button>`
					)

					$('.modal-body').append(elm)

					})
				})
		})

		$(document).on('click','.request-answer', function(){

			var id 		= $(this).data('id')
			var answer  = $(this).data('answer')

			$(this).parent().remove()

			var data = {

				action : "request-answer",
				target : id,
				answer : answer
			}
			Global.request('post','server.php',data)
				.then(function(r){

					console.log(r)
				})
		})

	}()
}


var profile = new Profile();

