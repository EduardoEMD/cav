var Controller = function(){

	this.request = function (type,url,data = {}, dataType = null){

		var options = { type:type, url: url, data: data }

		if(dataType) options.dataType = dataType

		return $.ajax(options)
	}
}

var Global = new Controller()