<?php session_start();

if (isset($_SESSION['user']) && !empty($_SESSION['user'])){ ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/profile.css">
</head>
<body>
	
	<h1 align="center"> <?php echo " Welcome " . $_SESSION['user']['nickname']; ?> </h1>
	<button type="button" id="requests" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Requests</button>
	<div class="content">
		<div class="most-body">
			
		</div>
		<div class="aside">
			<input placeholder="search" type="text" class="form-control search_input" id="search_button">
			<div class="searching_div">
			</div>
		</div>
	</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="assets/controller.js"></script>
<script src="assets/profile.js"></script>
</html>



<?php 

}else{

	header('location: login.php');
} 

?>