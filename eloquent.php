<?php 

namespace Eloquent;

class Database{

	protected $conn;
	protected $table;

	public function __construct($x,$y,$z){

		$this->conn = new \mysqli($x,$y,$z);

	}

	public function set_database($dbname){

		$this->conn->select_db($dbname);
	}

	public function findAll(){

		return $this->conn->query("SELECT * FROM $this->table")->fetch_all(MYSQLI_ASSOC);
	}

	public function set_table($tblname){

		$this->table = $tblname;
	}


	public function insert($arr){

		$keys 	= "";
		$values = ""; 

		foreach($arr as $key => $value){

			$keys 	.= "$key,";
			$values .= "'$value',";
		}

		$keys 	= substr($keys, 0, -1);
		$values  = substr($values, 0, -1);

		return $this->conn->query("INSERT INTO $this->table($keys) VALUES($values)");

	}

	public function find(Array $arr, $match = false){

		$statement 	= "";

		if(!$match){

			foreach($arr as $key => $value){

				$statement 	.= " $key = '$value' and";
			}

		}else{

			foreach($arr as $key => $value){

				$statement 	.= " $key like '%$value%' and";
			}
		}


		$statement = substr($statement, 0, -4);

		return $this->conn->query("SELECT * FROM $this->table WHERE $statement")->fetch_all(MYSQLI_ASSOC);

	}

	public function delete(Array $arr){
		
		$statement 	= "";

		foreach($arr as $key => $value){

			$statement 	.= " $key = '$value' and";
		}

		$statement = substr($statement, 0, -4);

		return $this->conn->query("DELETE  FROM $this->table WHERE $statement");

	}


	public function find_match($target, Array $arr){

		$statement = "";

		for($i = 0; $i < count($arr); $i++){
			$statement .= " '$arr[$i]',";
		}

		$statement = substr($statement, 0, -1);
		$statement = "(".$statement.")";

		return $this->conn->query("SELECT * FROM $this->table WHERE $target IN $statement")->fetch_all(MYSQLI_ASSOC);
	}



	

}



 ?>